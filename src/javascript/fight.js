export function fight(firstFighter, secondFighter) {
  
  let healthFirst = firstFighter.health;
  let healthSecond = secondFighter.health;

  do{
      healthSecond -= getDamage(firstFighter, secondFighter);
      healthFirst -= getDamage(secondFighter, firstFighter);
  }while(healthFirst >= 0 && healthSecond >= 0)

  return healthFirst > healthSecond ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  
  let damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage > 0 ? damage: 0;
}

export function getHitPower(fighter) {
  
    let criticalHitChance = Math.random() + 1;
    let hitPower = fighter.attack * criticalHitChance;
    return hitPower;
}

export function getBlockPower(fighter) {
  
  let dodgeChance = Math.random() + 1;
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}
