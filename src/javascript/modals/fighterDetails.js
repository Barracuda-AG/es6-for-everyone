import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense , source} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({tagName: 'p', className: 'fighter-health' });
  const attackElement = createElement({tagName: 'p', className: 'fighter-attack' });
  const defenseElement = createElement({tagName: 'p', className: 'fighter-defense' });
  let attributes = { src: source };
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  nameElement.innerText = "Name: " + name;
  healthElement.innerText ="Health: " + health;
  attackElement.innerText = "Attack: " + attack;
  defenseElement.innerText = "Defense: " + defense;

  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
