import { showModal } from "./modal";
import { createElement } from '../helpers/domHelper';

export  function showWinnerModal(fighter) {
    
  const { name, source } = fighter;
  var attributes = {src : source};

  const fighterDetails = createElement({ tagName: 'p', className: 'modal-body' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  fighterDetails.append(imageElement);
  
  showModal({title: `Winner is:  ${name}`, bodyElement: fighterDetails});

}